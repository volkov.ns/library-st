insert into flowers (title, price)
values ('Роза', 100);
insert into flowers (title, price)
values ('Лилия', 50);
insert into flowers (title, price)
values ('Ромашка', 25);

insert into customers (firstName, phone)
values ('Алексей', '+74951111111');
insert into customers (firstName, phone)
values ('Иван', '+74952222222');
insert into customers (firstName, phone)
values ('Сергей', '+74953333333');


insert into orders (fl_id, cust_id, date_added, quantity)
values (2, 4, now() - interval '45d', 9);
insert into orders (fl_id, cust_id, date_added, quantity)
values (1, 5, now() - interval '24h', 33);
insert into public.Orders (fl_id, cust_id, date_added, quantity)
values (2, 5, now() - interval '40d', 20);
insert into orders (fl_id, cust_id, date_added, quantity)
values (3, 6, now(), 101);