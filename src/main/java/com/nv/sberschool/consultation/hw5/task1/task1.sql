create table flowers (
    id serial primary key,
    title varchar(30) not null,
    price integer not null
);

create table if not exists customers --  if not exists - -----------
(
   id serial primary key,
   firstName varchar(30) not null,
   phone varchar(20) not null
);

 create table if not exists orders
(
   id  serial primary key,
   fl_id integer references flowers,
   cust_id integer references customers,
   date_added timestamp not null,
   quantity integer not null check (quantity between 1 and 1000)
 );