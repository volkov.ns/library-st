--По идентификатору заказа получить данные заказа и данные клиента,
--создавшего этот заказ
select o.*, c.firstname as customer_name, c.phone as customer_phone
from orders o
inner join customers c on o.cust_id = c.id
where o.id = 1;

--Получить данные всех заказов одного клиента по идентификатору
--клиента за последний месяц

select c.*, o.*
from customers c
inner join orders o on o.cust_id = c.id
where c.id = 5 and o.date_added >= now() - interval '30d';

--Найти заказ с максимальным количеством купленных цветов, вывести их
--название и количество

select f.title, o.quantity
from orders o
inner join flowers f on o.fl_id = f.id
where o.quantity = (select max(quantity) from orders);


--Вывести общую выручку (сумму золотых монет по всем заказам) за все
--время
select sum(o.quantity * f.price)
from orders o
inner join flowers f on o.fl_id = f.id;