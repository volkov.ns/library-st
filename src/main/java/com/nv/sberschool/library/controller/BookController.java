package com.nv.sberschool.library.controller;

import com.nv.sberschool.library.model.Author;
import com.nv.sberschool.library.model.Book;
import com.nv.sberschool.library.repository.AuthorRepository;
import com.nv.sberschool.library.repository.BookRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.webjars.NotFoundException;

@RestController
@RequestMapping("/books")
@Tag(name = "Книги", description = "Контроллер для работы с книгами библиотеки")
public class BookController extends GenericController<Book> {

    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;
    public BookController(BookRepository bookRepository, AuthorRepository authorRepository) {
        super(bookRepository);
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
    }

    @Operation(description = "Добавить автора к книге", method = "addAuthor")
    @RequestMapping(value = "/addAuthor", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Book> addAuthor(@RequestParam(value = "authorId") Long authorId,
                                            @RequestParam(value = "bookId") Long bookId) {
        try {
            Book book = bookRepository.findById(bookId).orElseThrow(() -> new NotFoundException("Книга с переданным ID не найдена"));
            Author author = authorRepository.findById(authorId).orElseThrow(() -> new NotFoundException("Автор с переданным ID не найден"));
            book.getAuthors().add(author);
            bookRepository.save(book);
            return ResponseEntity.status(HttpStatus.OK).body(book);
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
