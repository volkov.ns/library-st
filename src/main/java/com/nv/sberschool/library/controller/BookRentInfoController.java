package com.nv.sberschool.library.controller;

import com.nv.sberschool.library.model.BookRentInfo;
import com.nv.sberschool.library.repository.BookRentInfoRepository;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rent/info")
@Tag(name = "Аренда книг",
        description = "Контроллер для работы с арендой/выдачей книг пользователям библиотеки")
public class BookRentInfoController extends GenericController<BookRentInfo> {
    public BookRentInfoController(BookRentInfoRepository bookRentInfoRepository) {
        super(bookRentInfoRepository);
    }
}
