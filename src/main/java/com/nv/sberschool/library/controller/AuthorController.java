package com.nv.sberschool.library.controller;

import com.nv.sberschool.library.model.Author;
import com.nv.sberschool.library.model.Book;
import com.nv.sberschool.library.repository.AuthorRepository;
import com.nv.sberschool.library.repository.BookRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.webjars.NotFoundException;

@RestController
@RequestMapping("/authors")
@Tag(name = "Авторы", description = "Контроллер для работы с авторами книг библиотеки")
public class AuthorController extends GenericController<Author> {

    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;

    public AuthorController(AuthorRepository authorRepository, BookRepository bookRepository) {
        super(authorRepository);
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
    }


    @Operation(description = "Добавить книгу к автору", method = "addBook")
    @RequestMapping(value = "/addBook", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Author> addBook(@RequestParam(value = "bookId") Long bookId,
                                          @RequestParam(value = "authorId") Long authorId) {
        try {
            Book book = bookRepository.findById(bookId).orElseThrow(() -> new NotFoundException("Книга с переданным ID не найдена"));
            Author author = authorRepository.findById(authorId).orElseThrow(() -> new NotFoundException("Автор с переданным ID не найден"));
            author.getBooks().add(book);
            authorRepository.save(author);
            return ResponseEntity.status(HttpStatus.OK).body(author);
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
